import React, { Component } from "react";
import { connect } from "react-redux";
import "./App.css";
import { actSelectData } from "./actions";
import Result from "./components/Result";

class App extends Component {
  constructor() {
    super();
    this.state = {
      arr: [],
      search: "",
      data: [
        {
          name: "Apple",
          show: false
        },
        {
          name: "SamSung",
          show: false
        },
        {
          name: "HTC",
          show: false
        },
        {
          name: "Sony",
          show: false
        }
      ],
      isShow: true
    };
  }

  onChange = event => {
    //const name = event.target.name;
    const value = event.target.value;
    const check = event.target.checked;
    const values = this.state.arr;
    if (check === true) {
      values.push(value);
    } else {
      for (var i = 0; i < values.length; i++) {
        if (values[i] === value) {
          values.splice(i, 1);
        }
      }
    }
    this.props.onChangeData(values);
  };

  onReset = () => {
    const checkboxes = document.getElementsByClassName("checked");
    for (let i = 0; i < checkboxes.length; i++) {
      checkboxes[i].checked = false;
    }
    this.props.onChangeData([]);
  };

  //--------------------
  onSearch = event => {
    var { value } = event.target;
    //value.toLowerCase()
    //console.log(value.toLowerCase())
    var { data } = this.state;
    data.forEach(e => {
      if (e.name.toLowerCase().indexOf(value.toLowerCase()) > -1) {
        e.show = false;
      } else e.show = true;
    });
    this.setState({});
    value = value.toLowerCase();
  };
  //hàm đước gọi khi prop thay đổi
  componentWillReceiveProps(nextProp) {
    this.setState({
      arr: nextProp.data.checklists.data
    });
  }
  // hàm được gọi lại khi state thay đổi
  // ------------------------------

  render() {
    const data = this.state.data;

    const showlist = data.map((e, index) => {
      return (
        <li key={index} hidden={e.show}>
          <label className="container-check">
            <input
              className="checked"
              name="name"
              value={e.name}
              type="checkbox"
              onChange={this.onChange}
            />
            {e.name}
            <span className="checkmark"></span>
          </label>
        </li>
      );
    });

    return (
      <div className="section">
        <div className="main">
          <div className="main-left m-t">Search option:</div>
          <div id="btndropdown" className="main-right">
            <div className="showform">
              <div className="showform">
                <div
                  className="btn-dropdown"
                  tabindex="1"
                  onClick={() => this.setState({ isShow: !this.state.isShow })}
                >
                  <label className="small">status</label>
                  <label id="demo" className="m-10">
                    <Result />
                  </label>
                </div>
                <div
                  hidden={this.state.isShow}
                  className="form-check-dropdown"
                  tabindex="1"
                >
                  <p className="p-text"> Status</p>
                  <div className="dropDown">
                    <input
                      name="search"
                      type="text"
                      placeholder="nhập tìm kiếm"
                      className="search"
                      id="search"
                      onChange={this.onSearch}
                    />
                    <div className="form-check">
                      <ul id="listData">{showlist}</ul>
                    </div>
                    {/* <div className="result">
                      <p className="show-result">
                        Result :
                        {this.state && this.state.arr.length === 0
                          ? null
                          : this.state.arr.map(e => {
                              return (
                                <div>
                                  <strong>{e}</strong>
                                </div>
                              );
                            })}
                      </p>
                    </div> */}
                    {/* -------------------------------- */}
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="main-left center">Result:</div>
          <div className="main-right">
            <div className="select-form-result">
              <p className="result">
                <Result />
                <div onClick={this.onReset} className="reset-result">
                  <img
                    alt="hahah"
                    src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAAABmJLR0QA/wD/AP+gvaeTAAAC0ElEQVRYhe1Xy04UQRQ9VRmXZKIiCyJMiGxk2RQDCwQHE/7A8A2QuJSNDwLDRuNS/QVjiB8gC1vEDUl1L2VlJojROIAEhwQSOnVc0CTTPVU9zSNhw0lmMVX33nOq69yuauAKlwxxmmDf9wvFYnGc5CTJ2yRvA4AQ4mf8W97b21upVCrRhQrQWhdJzgohpgFcbxO+K4R4e3R09HJkZOTfuQUEQTBF8jWAzjxim7ANYEYptZQVJF0TJIXWukry3RnIEee811ovkHQutOCaCIJgAcDTMxA3QwB4FgQBAcy5AmzkU/HKT2XSDBDAlG07Wgi01kUA3wHcvCDyE2xHUXQnbcwWD5CczSD3AdQzSOpxjA2dhULhcXowIUBrfS1uNRs+K6UmpJRlADXLfE1KWVZKTQghVhw1ZnzfT/guIUBKOQZ3nw+EYVjyPG9DSllJiahJKSue522EYVgiOeCocaOjo+OeUwDJSUciAHQZY3yLiAS5McYHcCujToIj7YHejEQA6EuLsJD3tamR4EjshzGmW4i2nddnjPmytrZ23/O8GgBorXuNMZ9ykANAd/OftAeYo0ALjDHEca/nQSIu7YFfOQrUpJRjw8PDtTAMS2EYlsrl8qaU8gHs3ZHGb6cAAD9ykCf23GHMLCQ4EgKEEMsZiXWH4VqMCWDLVYTkR6cAkqsAdh253zLcnhBBct1RY2d/f/9r80CL5YMgWCT5xJYthFgheRdAl4OgLoRYJznumK8qpZ43D7ScBQcHBy8A/LFlx4Vd5ADQlUG+FUXRq/Rgi4DR0dEGgEfI31Z5QJLTtiua9UYUn9uLF8i+MDQ09ME257ySDQ4OzgGo4nxPgiTnlVLzroC2712t9UMAb5B9wNhQJznjWvkJnE/gBEqppSiK+nG8JX9zEO8AqB4eHva3IwfO8GESn+eTAHpI9gCAEGITwCaA5UajsXqaD5MrXDr+AymFZSdcfZVPAAAAAElFTkSuQmCC"
                  />
                </div>
              </p>
              <button onClick={this.onReset} className="btn-reset">
                <img
                  alt="reset"
                  src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAABmJLR0QA/wD/AP+gvaeTAAAA9UlEQVQ4jdXSMS9DYRTG8d8tbSwSg6T4BBKTCL6AxSAdiEUiaSR8AGa5acx2BltX6dXBLLFgYJD4CFIJk0Go+1rapmnVHRv/8XnPeXLOc16GTdQrnJCfZjtQxiwKgUdUG5zOsAxrXMNod3OdyZTLwGsgTrlv0iywGHEwxU7KDRpaBh1icgm3CZVB4yZUEkJC3NY6E8yzjvcShwOaQ0YcJIxnFv1/etfMdT3ErYRDRmD1Ght9Bl3EpV8+GFxwhLEHzttap7B12yKW8ILjlLvASJ6Fb/ZzTHywuslbn0GNFWhwVWQXWxFz+MRTxNkz1T2+/lhvCPwAN9dBlJ+jmc4AAAAASUVORK5CYII="
                />
                Reset
              </button>
            </div>
          </div>
        </div>
        {/* <from className="main-form">
          <div className="dropDown">
            <input
              name="search"
              type="text"
              placeholder="nhập tìm kiếm"
              className="search"
              onChange={this.onSearch}
            />
            <div className="form-check">
              <ul id="listData">{showlist}</ul>
            </div>
            <div className="result">
              <p className="show-result">
                Result :
                 {this.state && this.state.arr.length === 0
                  ? null
                  : this.state.arr.map(e => {
                      return (
                        <div>
                          <strong>{e}</strong>
                        </div>
                      );
                    })}
              </p>
              <button onClick={this.onReset}>Reset</button>
            </div>
          </div>
          <Result />
        </from> */}
      </div>
    );
  }
}
//hàn thực hiện gọi action
const mapDispatchToProps = dispatch => {
  return {
    //khai báo hàm onChangeData để có thể gọi được hàm này ra ta phải đặt this.prop ở đầu
    onChangeData: data => {
      // khi gọi vào cần dispatch action để gọi sự kiện đã viết trong action  => action
      dispatch(actSelectData(data));
    }
  };
};

const mapStatetoProps = data => {
  return {
    data: data
  };
};

export default connect(
  mapStatetoProps,
  mapDispatchToProps
)(App);
