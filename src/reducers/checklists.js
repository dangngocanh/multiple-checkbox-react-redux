import * as Types from "../constants/Actiontype";
const initialState = [];
//khi bắt được action thay đổi sẽ gọi vào hàm này  để trả ra dữ liệu ngược lại và và thay đổi trong store
//các giá trị sẽ được lưu lại để lấy ra ta  sư dụng hàm mapStatetoProps trong component muốn lấy ra
const checklists = (state = initialState, action) => {
  switch (action.type) {
    case Types.ADD_CHECKLIST:
      state.data = action.data;
      return { ...state };
    default:
      return { ...state };
  }
};

export default checklists;
