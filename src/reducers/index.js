import { combineReducers } from "redux";
import checklists from "./checklists";
const appReducers = combineReducers({
  checklists
});
export default appReducers;
