import React, { Component } from "react";
import { connect } from "react-redux";
class Result extends Component {
  constructor(props) {
    super(props);
    this.state = {
      arr: []
    };
  }

  componentDidMount() {
    console.log(this.props);
  }
  componentWillReceiveProps(nextProps) {
    //console.log("ok", nextProps);
    console.log(nextProps.data.checklists.data.length);

    var arr = nextProps.data.checklists.data.map((e, index) => {
      return (
        <div className="list-item">
          {e}
          {index === nextProps.data.checklists.data.length - 1 ? "" : ","}
        </div>
      );
    });
    //console.log(arr);
    this.setState({
      arr: arr
    });
  }

  render() {
    return <div className="resultBox">{this.state.arr}</div>;
  }
}

const mapStateToProps = data => {
  return {
    data: data
  };
};
export default connect(
  mapStateToProps,
  null
)(Result);
